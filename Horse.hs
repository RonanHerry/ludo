module Horse where

-- we need to be able to find back who is the owner of a horse
-- and to distinguish among horses
import qualified Data.IntMap.Strict as I
import qualified Data.IntMap.Strict.Extra as I
import qualified Data.PartialOrd as Partial

import Util

import Player

type Square = Int

data Position = Jail | Free Square | Home Int | Out deriving (Show, Eq)

instance Ord Position where
  Jail <= _ = True
  _ <= Out  = True
  (Free _) <= (Home _) = True
  (Free n) <= (Free m) = n <= m
  (Home n) <= (Home m) = n <= m
  p1 <= p2 = (p1 == p2) || (p2 > p1)


data Horse = Horse { owner    :: Player
                   , position :: Position
                   , number   :: Int } deriving (Show)

instance Eq Horse where
  h1 == h2 = (owner h1 == owner h2) && (number h1 == number h2)

instance Partial.PartialOrd Horse where
  h1 <= h2 = ((isInGame h1 && isInGame h2) || (owner h1 == owner h2)) && position h1 <= position h2

type Horses = I.IntMap Horse

isOut :: Horse -> Bool
isOut = (== Out) . position

isInJail :: Horse -> Bool
isInJail = (== Jail) . position

isInGame :: Horse -> Bool
isInGame h = case position h of Free _ -> True
                                _ -> False

isHome :: Horse -> Bool
isHome h = case position h of Home _ -> True
                              _ -> False

changePosition :: Horse -> Position -> Horse
changePosition h p = h { position = p }

haveSamePosition :: Horse -> Horse -> Bool
haveSamePosition = position === position
