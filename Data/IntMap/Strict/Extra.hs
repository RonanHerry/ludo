module Data.IntMap.Strict.Extra where

import Data.Maybe
import qualified Data.IntMap.Strict as I

key :: Eq a => a -> I.IntMap a -> Maybe I.Key
key v map = listToMaybe $ filter (keyCorresponds v map) (I.keys map)

keyCorresponds :: Eq a => a -> I.IntMap a -> I.Key -> Bool
keyCorresponds v map k = I.member k map && fromJust (I.lookup k map) == v
