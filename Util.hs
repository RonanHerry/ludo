module Util where

import Control.Applicative

(&=&) :: (a -> b -> Bool) -> (a -> b -> Bool) -> a -> b -> Bool
(&=&) = (liftA2 . liftA2) (&&)

(===) :: Eq a => (b -> a) -> (c -> a) -> (b -> c -> Bool)
f === g = flip ((==) . g) . f
