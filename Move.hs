module Move where

import Util
import Data.Maybe
import qualified Data.IntMap.Strict as I
import qualified Data.IntMap.Strict.Extra as I
import qualified Data.PartialOrd as P
import Text.Read

import GameParams
import Player
import Horse

data Move = Move { whichHorseMoves :: Horse
                 , whereTo  :: Position } deriving (Show)

type MoveSelection = I.IntMap [Move]

type Roll = Int

type Rolls = [Roll]

whoMoves :: Move -> Player
whoMoves = owner . whichHorseMoves

moveToHorse :: Move -> Horse
moveToHorse m = h { position = whereTo m }
  where h = whichHorseMoves m

moveRules :: Roll -> Position -> Maybe Position
-- get out of jail
moveRules 6 Jail = Just (Free 1)
-- move on the board
moveRules m (Free n)= Just (Free (if n+m <= sizeOfBoard then n+m else 2*sizeOfBoard-n-m))
  -- move on the ladder
moveRules m (Home n)
    | m == (n+1) = Just (Home m)
    | otherwise = Nothing
moveRules _ _ = Nothing

maybeMove :: Horse -> Maybe Position -> Maybe Move
maybeMove h Nothing = Nothing
maybeMove h (Just p) = Just $ Move h p

horseMove :: Horse -> Roll -> Maybe Move
horseMove h r = maybeMove h (moveRules r (position h))

canBeBlockedBy :: Move -> Horse -> Bool
m `canBeBlockedBy` h = (whichHorseMoves m P.< h) && (h P.< moveToHorse m)

allLegalHorseMoves :: Rolls -> Horses -> MoveSelection
allLegalHorseMoves rs = fmap (catMaybes . flip g rs)
  where g h = fmap (horseMove h)

canBeCapturedBy :: Horse -> Move -> Bool
canBeCapturedBy =  (position === whereTo) &=& (owner === whoMoves)

readInt :: String -> Maybe Int
readInt = readMaybe

thereAreNoMoves :: MoveSelection -> Bool
thereAreNoMoves = I.null

readMove :: MoveSelection -> String -> Maybe [Move]
readMove mvs playerInput = readInt playerInput >>= flip I.lookup mvs
