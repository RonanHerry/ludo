module Game
  ( module Game
  , module Player
  , module Move
  , module Horse
  ) where

import Data.Maybe
import Data.List
import qualified Data.Map.Strict as M
import qualified Data.IntMap.Strict as I
import qualified Data.IntMap.Strict.Extra as I

import Player
import Horse
import Move

nbPlayers :: Int
nbPlayers = 2
nbHorses  :: Int
nbHorses = 4
nbBlocking = 4

type Players = I.IntMap Player

data Game = Game { players :: I.IntMap Player
                 , horses :: M.Map Player Horses
                 , currentPlayer :: Player } deriving (Show, Eq)

instance Ord Player where
  compare p1 p2 = compare (color p1) (color p2)

horsesList :: Game -> [Horse]
horsesList g = concatMap I.elems (M.elems $ horses g)

play :: Move -> Game -> Game
play m g = g { horses = replacesHorses (givePlayerAndHorses m g) (horses g) }

replacesHorses :: (Player, Horses) -> M.Map Player Horses -> M.Map Player Horses
replacesHorses = uncurry M.insert

givePlayerAndHorses :: Move -> Game -> (Player, Horses)
givePlayerAndHorses m g = (whoMoves m, I.insert (number $ whichHorseMoves m) (moveToHorse m) (horsesOf (whoMoves m) g))

nextPlayer :: Game -> Game
nextPlayer g = g { currentPlayer = fromJust $ I.lookup k (players g) }
  where k' = fromJust (order (currentPlayer g) g) + 1
        k | k' > nbPlayers = 1
          | otherwise = k'

order :: Player -> Game -> Maybe I.Key
order p g = I.key p (players g)

isGameOver :: Game -> Bool
isGameOver = isJust . winner

hasWon :: Game -> Player -> Bool
hasWon g p = I.size (I.filter isOut (horsesOf p g)) == nbHorses

horsesOf :: Player -> Game -> Horses
horsesOf p g = fromJust $ M.lookup p (horses g)

winner :: Game -> Maybe Player
winner g = find (hasWon g) (players g)

legalMoves :: Player -> Rolls -> Game -> MoveSelection
legalMoves p rs g = I.filter (not . null) $ fmap (filter (`isNotBlocked` g)) (allLegalHorseMoves rs (horsesOf p g))

-- hasNoMoves :: Player -> Rolls -> Game -> Bool
-- hasNoMoves p rs g = I.null $ legalMoves p rs g

-- horseMove :: Game -> Roll -> Horse -> Maybe Move
-- horseMove g r h = undefined
 
initHorses :: Player -> Horses
initHorses p = I.fromList [ (i, Horse { owner = p, position = Jail, number = i }) | i <- [1..nbHorses]] 

initPlayer :: (String, Color) -> Player
initPlayer (s, c) = Player { name = s, color = c }

ronan = initPlayer ("Ronan", Red)
buya  = initPlayer ("Buya", Green)
testGame :: Game
testGame = Game { players = ps', horses = hs, currentPlayer = ronan }
  where ps = [ronan, buya]
        hs = M.fromList [ (p, initHorses p) | p <- ps ]
        ps' = I.fromList [(1,ronan),(2,buya)]

-- haveSamePosition :: Horse -> Horse -> Bool
-- haveSamePosition h1 h2 = position h1 == position h2

-- blockedBy :: Game -> Player -> [Position]
-- blockedBy g p = fmap position $ concat $ filter ((>= nbBlocking) . length) $ groupBy haveSamePosition $ filter isInGame (I.elems hs)
--   where hs = fromJust $ M.lookup p (horses g)

canBeBlocking :: Move -> Game -> [Horse]
canBeBlocking m g = filter (m `canBeBlockedBy`) (horsesList g)

blocking :: Move -> Game -> [[Horse]]
blocking m g = filter ((== nbBlocking) . length) (groupBy haveSamePosition (canBeBlocking m g))

isNotBlocked :: Move -> Game -> Bool
isNotBlocked m g = null $ blocking m g

isBlocked :: Move -> Game -> Bool
isBlocked m g = not $ isNotBlocked m g

-- allPlayersExcept :: Game -> Player -> [Player]
-- allPlayersExcept g p = delete p (players g)

-- legalMoves :: Game -> Player -> Dice -> [Move]
-- legalMoves = undefined


-- hasDouble :: DiceRoll -> Bool
-- hasDouble = anySame

(|=) :: a -> Maybe b -> Maybe (a,b)
_ |= Nothing = Nothing
x |= Just y  = Just (x,y)

-- moveHorse :: Horse -> Dice -> Maybe Move
-- moveHorse h d = h |= moveHorse' h d where
--   -- get out of jail
--   moveHorse' Jail 6 = Just (Free 1)
--   -- move on the board
--   moveHorse' (Free n) m = Just (Free (if n+m <= sizeOfBoard then n+m else 2*sizeOfBoard-n-m))
--   -- move on the ladder
--   moveHorse' (Home n) m
--     | m == (n+1) = Just (Home m)
--     | otherwise = Nothing
--   moveHorse' _ _ = Nothing

-- movable :: Horse -> Dice -> Bool
-- movable h d = isJust (moveHorse h d)

-- moves :: Player -> Dice -> [Move]
-- moves p d = nub $ catMaybes $ fmap (flip moveHorse d) (horses p)

-- -- move :: Player -> Move -> Player
-- -- move p m = Player { name = name p, color = color p , horses = change m (horses p) }
-- --   where change m hs = uncurry replace (fmap Util.singleton m) hs
