module Player (module Player) where

data Color = Red | Green | Blue | Yellow deriving (Show, Read, Enum, Eq, Ord)

data Player = Player { name   :: String
                     , color  :: Color } deriving (Show, Eq)
