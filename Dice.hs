module Dice where

import System.Random
import Control.Monad.Random
import Control.State

type Dice = Int

data DiceRoll = Doublet Int | Normal Int Int deriving (Show)

--d k rolls a dice k and update the random generator
d :: MonadRandom m => Int -> m Dice
d n = getRandomR (1,n)

--roll n $ d k rolls n d k
roll :: MonadRandom m => Int -> m Dice -> m [Dice]
roll = replicateM --replicateM = sequence . replicate


-- rollDie :: RandomGen g => State g Int
-- rollDie = State $ randomR (1,6)

-- dice :: RandomGen g => g -> [Int]
-- dice = evalState (sequence . repeat $ rollDie) 
