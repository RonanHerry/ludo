module Control.State where

import Prelude hiding (id, (.))

import Data.Functor
import Control.Applicative
import Control.Monad

newtype State s a = State { runState :: s -> (a, s) }

instance Functor (State s) where
  fmap = Control.Monad.liftM

instance Applicative (State s) where
  pure = return
  (<*>) = Control.Monad.ap

instance Monad (State s) where
  return x = State ( \ s -> (x, s) )
  p >>= k = State $ \ s0 ->
    let (x, s1) = runState p s0
    in runState (k x) s1     

put newState = State $ const ((), newState)

get = State $ \s -> (s, s)

evalState :: State s a -> s -> a
evalState p s = fst (runState p s)

execState :: State s a -> s -> s
execState p s = snd (runState p s)
