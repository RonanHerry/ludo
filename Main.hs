import Data.Maybe
import Control.Monad.Trans.Class
import Control.Monad.Trans.State

import Game
import Dice

getMoves :: MoveSelection -> IO [Move]
getMoves options
  | thereAreNoMoves options = do
    putStrLn "You have no legal moves"
    pure []
  | otherwise =  do
    putStrLn "Your move options:"
    sequence_ (fmap print options)
    putStrLn "Indicate move"
    move <- getLine
    maybe (do putStrLn "Invalid move"
              getMoves options) pure (readMove options move)

printGame :: StateT Game IO ()
printGame = do
  board <- get
  lift $ print board

initialGame :: Game
initialGame = testGame

-- game params
nbDice :: Int
nbDice = 2
maxDice :: Int
maxDice = 6

compose :: [a -> a] -> a -> a
compose = foldr (.) id

gameLoop :: IO ()
gameLoop = evalStateT loop initialGame
  where
    loop = do
      -- printGame
      playerToMove <- gets currentPlayer
      (lift . putStrLn) $ name playerToMove ++ " to move"
      rolls <- roll nbDice $ d maxDice
      (lift . putStrLn) $ "You rolled " ++ show rolls
      moveOptions <- gets (legalMoves playerToMove rolls)
      moves <- lift $ getMoves moveOptions
      modify (compose (fmap play moves))
      modify nextPlayer
      done <- gets isGameOver
      if done then pure () else loop

main :: IO ()
main = gameLoop
